import path from 'path';
import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';

import htmlMiddleware from './middleware/html';
import renderMiddleware from './middleware/render';
import { authMiddleware } from './middleware/auth';
import * as api from './api';

const publicPath = path.join(__dirname, '/public');
const app = express();

app.use(cookieParser());
app.use(bodyParser.json());
app.use(authMiddleware)
app.use('/authVk', api.authVk)

app.use('/authSuccess', api.getAccessTokenVk)
app.use('/api/getUser', api.getUser)
app.use('/api/getFriends', api.getFriends)
app.use('/api/getMutual', api.getMutual)

app.use(express.static(publicPath));
app.use(htmlMiddleware());
app.use(renderMiddleware());


export default app;
