import * as vk from '../vk';

export const authMiddleware = (req, res, next) => {
    const token = req.cookies[vk.AUTH_COOKIE_KEY];

    console.log(token);

    req.authToken = token;
    next()
}