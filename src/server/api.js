import url from 'url';

import * as vk from './vk';
import config from '../config';


const HOME_PAGE = 'localhost:3000';

export const authVk = (req, res) => {
    return res.redirect(vk.createAuthLink())
};

export const getAccessTokenVk = async (req, res) => {
    const result = await vk.getAccessToken(req.query.code);
    console.log(result);

    res.cookie(vk.AUTH_COOKIE_KEY, result.access_token, { maxAge: 900000, httpOnly: true })

    res.redirect(config.SELF_HOST);
};

export const getUser = (req, res, next) => {
    const { body = {} } = req;
    const userId = body.userId;

    vk.get(userId, req.authToken, 'users.get', 'GET')
        .then(function (response = []) {
            res.json({ result: response[0] });
        })
        .catch(function (error) {
            return next(error);
        });
};

export const getFriends = (req, res, next) => {
    console.log('REQ BODY', req.body);
    const userId = req.body.userId;

    vk.get(userId, req.authToken, 'friends.get', 'POST')
        .then(function (response) {
            res.json({ result: response });
        })
        .catch(function (error) {
            return next(error);
        });
};

export const getMutual = (req, res, next) => {
    console.log(req.body)
    const friendsIds = req.body.friendsIds;

    vk.getMutual(req.authToken, friendsIds)
        .then(function (response) {
            res.json({ result: response });
        })
        .catch(function (error) {
            return next(error);
        });
};