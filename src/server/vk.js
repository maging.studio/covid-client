import request from 'request';
import async from 'async';
import _ from 'lodash';
import url from 'url';

export const VK_API_URL = 'https://api.vk.com/method/';
export const API_VERSION = '5.60';
export const VK_AUTH_URL = 'oauth.vk.com/';
export const APP_ID = '7370323';
export const AUTH_REDIRECT_URL = 'http://localhost:3000/authSuccess';
export const SECURE_SECRET = 'RIhNAJVhFqnDjJoCMqvh';
export const VK_MAX_REQUESTS_PER_SECOND = 7;
export const FRIENDS_IN_REQUEST = 50;
export const AUTH_COOKIE_KEY = 'vk_token';

export const createAuthLink = () => {
    return url.format({
        host: VK_AUTH_URL + 'authorize',
        protocol: 'https:',
        query: {
            client_id: APP_ID,  // jshint ignore:line
            redirect_uri: AUTH_REDIRECT_URL, // jshint ignore:line
            scope: 'friends',
            display: 'popup',
            response_type: 'code', // jshint ignore:line
            v: API_VERSION
        }
    });
};

export const createAccessTokenLink = ({ code, redirectUri }) => {
    return url.format({
        host: VK_AUTH_URL + 'access_token',
        protocol: 'https:',
        query: {
            code,
            client_secret: SECURE_SECRET,
            client_id: APP_ID,
            redirect_uri: redirectUri,
            v: API_VERSION
        }
    });
};

export const getAccessToken = (code) => {
    return new Promise((resolve, reject) => {
        request({  // jshint ignore:line
            url: 'https://' + VK_AUTH_URL + 'access_token',
            qs: {
                code,
                client_secret: SECURE_SECRET,
                client_id: APP_ID,
                redirect_uri: AUTH_REDIRECT_URL,
                v: API_VERSION
            },
        }, function (error, response, body) {
            if (error) {
                return reject(error);
            }
    
            return resolve(JSON.parse(body));
        });
    })
}

export const get = function (userId, token, url, method) {
    return new Promise(function (resolve, reject) {
        request({
            ...(userId ? { user_ids: userId } : {}),
            url: VK_API_URL + url,
            qs: {
                access_token: token,   // jshint ignore:line
                v: API_VERSION,
                fields: 'photo_50, sex, bdate'
            },
            method: method
        }, function (error, response, body) {
            if (error) {
                return reject(error);
            }

            return resolve(JSON.parse(body).response);
        });
    });
};

export const getMutual = function (token, friendsIds) {
    var friendsChunks = _.chunk(friendsIds, FRIENDS_IN_REQUEST);
    var tasks = [];

    friendsChunks.forEach(function (chunk) {
        tasks.push((callback) => {
            request({
                url: VK_API_URL + 'friends.getMutual',
                qs: {
                    access_token: token,   // jshint ignore:line
                    v: API_VERSION,
                    target_uids: chunk    // jshint ignore:line
                },
                method: 'POST'
            }, function (error, response, body) {
                if (error) {
                    return callback(error);
                }

                body = JSON.parse(body);

                if (body.error) {
                    return callback(new Error(body.error.error_msg)); // jshint ignore:line
                }

                return callback(null, body.response);
            });
        });
    });

    return new Promise(function (resolve, reject) {
        async.parallelLimit(tasks, VK_MAX_REQUESTS_PER_SECOND, function (error, results) {
            if (error) {
                return reject(error);
            }

            return resolve([].concat.apply([], results));
        });
    });
};