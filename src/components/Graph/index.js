import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import rd3 from 'react-d3-library';
import { getGraphData, getEmptyGraph } from './utils';
import { drawGraph } from './drawGraph';

const RD3Component = rd3.Component;

export const Graph = ({user, graph, setGraph}) => {
    const [error, onError] = useState(null);
    const [isBuilt, setIsBuilt] = useState(false);
    const [graphNode, setGraphNode] = useState(null);
    const [nodeOver, setNodeOver] = useState(null);

    useEffect(() => {
            getGraphData(graph, user.id)
                .then(updatedGraph => {
                    console.log('GRAPH', updatedGraph)
                    setGraph(updatedGraph);
                })
                .catch(error => {
                    console.log(error)
                    onError(error.message);
                })
        
    }, [user.id]) // eslint-disable-line

    const node = document.createElement('div');

    if (graph && !graphNode) {

        const Graph = drawGraph();

        Graph.draw(node, graph, user.id, {
            onMouseOver: (d) => setNodeOver(d),
            onMouseOut: () => setNodeOver(null)
        });

        setGraphNode(true)
    }

    return (
        <div style={{ width: '100%', 'height': '100%'}}>
            <div>
                {JSON.stringify(nodeOver)}
            </div>
            <span id='graph'></span>
            {/* <RD3Component data={graphNode} /> */}
        </div>
    )
}