
import httpBackend from '../../utils/httpBackend';

const buildGraph = (graph, userId) => {
    // me
    // friends
    // mutual
    // update
}

export const getEmptyGraph = () => ({
        directed: false,
        graph: [],
        nodes: [],
        links: [],
        multigraph: false
    })

const parseUserInfo = (user) => {
    return {
        id: user.id,
        name: user.first_name + ' ' + user.last_name,   // jshint ignore:line
        sex: user.sex,
        photo: user.photo_50    // jshint ignore:line
    };
};

const addFriendToGraph = (graph, userIndex, friend) => {
    graph.nodes.push(parseUserInfo(friend));

    graph.links.push({
        source: userIndex,
        target: friend.id
    });
};

export const getGraphData = async (oldGraph, userId) => {

    console.log('odosdosdo')
    const graph = JSON.parse(JSON.stringify(oldGraph));

    const { result } = await httpBackend('/api/getUser', { body: { userId } });
    
    const me = parseUserInfo(result);

    graph.nodes.push(me);

    const { result: { items: friends } } = await httpBackend('/api/getFriends', { body: { userId: userId } })
    
    console.log('Successfully get ' + friends.length + ' friends for ' + me.name);
    
    friends.forEach((friend) => {
        console.log('FRIEND', friend)
        addFriendToGraph(graph, userId, friend);
    });

    console.log('FRIENDS', friends);

    const friendsIds = friends.map(function (friend) {
        if (!friend.deactivated) {
            return friend.id;
        }
    });

    const { result: mutuals  } = await httpBackend('/api/getMutual', {body: { userId: userId, friendsIds: friendsIds }} );

    let linksCounter = 0;
    const linksMap = {};

    mutuals.forEach(function (friend) {
        friend.common_friends.forEach(function (target) {    // jshint ignore:line

            if (!linksMap[friend.id + ',' + target] && !linksMap[target + ',' + friend.id]) {
                linksMap[friend.id + ',' + target] = true;
                graph.links.push({
                    source: friend.id,
                    target: target
                });
                linksCounter += 1;
            }
        });
    })

    console.log('Successfully get ' + linksCounter + ' links');

    return graph;
}