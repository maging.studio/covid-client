import React, { useState, useEffect } from "react";

import logo from "./logo.svg";
import { Graph } from "./components/Graph";
import { getEmptyGraph } from "./components/Graph/utils";

import httpBackend from "./utils/httpBackend";

import "./App.css";

function App() {
  const [user, setUser] = useState(null);
  const [graph, setGraph] = useState(getEmptyGraph());
  const [error, setError] = useState(null);
  const [authPassed, setAuthPassed] = useState(true);

  const showError = error => {
    setError(error);
  };

  useEffect(() => {
    httpBackend("/api/getUser")
      .then(({ result }) => {
        if (result) {
          setUser(result);
        }
      })
      .catch(showError);
  }, [authPassed]);

  return (
    <div className="App">
      <header className="App-header">
        {user ? <span>{user.id}</span> : <a href="/authVk">Войти</a>}
      </header>

      {/* {user && 
              <Graph
              user={user}
              graph={graph}
              setGraph={setGraph}
              showError={showError}
            />
      } */}
    </div>
  );
}

export default App;
